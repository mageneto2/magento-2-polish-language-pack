<?php
/**
 * @author Marceli Podstawski <marceli.podstawski@gmail.com>
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::LANGUAGE,
    'macpain_pl_pl',
    __DIR__
);